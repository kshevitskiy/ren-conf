<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Renaissance_Conf
 */

?>

<?php
	$homepage = '';
	if ( is_front_page() ) :
		$homepage = 'homepage';
	endif;
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico/r.e.n-favicon.png" type="image/png">
	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K34FS55');</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K34FS55"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
	<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'renaissance-conf' ); ?></a>

	<header id="masthead" class="site-header">
		<nav id="site-navigation" class="navbar navbar-expand-lg navbar-dark bg-faded main-navigation align-items-start">
			<div class="container">

				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="site-branding <?php echo $homepage; ?>">
					<?php
					the_custom_logo();
					if ( is_front_page() && is_home() ) : ?>
						<h1 class="navbar-brand"><?php bloginfo( 'name' ); ?></h1>
					<?php else : ?>
						<span class="navbar-brand"><?php bloginfo( 'name' ); ?></span>
					<?php
					endif;

					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description sr-only"><?php echo $description; /* WPCS: xss ok. */ ?></p>
					<?php
					endif; ?>
				</a><!-- .site-branding -->

				<div class="div nav-toggle-wrapper ml-auto">
					<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse flex-column-reverse align-items-end" id="navbarNav">
					<div class="site-header-navbar-wrapper">
						<?php
							wp_nav_menu( array(
								'theme_location'  => 'menu-1',
								'menu_id'         => 'primary-menu',
								'menu_class'      => 'navbar-nav text-uppercase',
								'container'		  => 'ul'
							) );
						?>
					</div>

					<div class="site-header-topbar-wrapper">
						<div class="d-flex flex-column align-items-start flex-lg-row align-items-lg-center">
							<div class="contact-info">
								<?php if (get_locale() == 'en_GB') { ?>
									<span>
										phone: <a href="tel:+48221647000">+48 22 164 7000</a>
										e-mail: <a href="mailto:konferencje@renaissance.waw.pl">konferencje@renaissance.waw.pl</a>
										<a href="http://www.renaissancewarszawa.pl/" target="_blank">renaissancewarszawa.pl</a>
									</span>
								<?php
								} else {
								?>
									<span>
										telefon: <a href="tel:+48221647000">+48 22 164 7000</a>
										e-mail: <a href="mailto:konferencje@renaissance.waw.pl">konferencje@renaissance.waw.pl</a>
										<a href="http://www.renaissancewarszawa.pl/" target="_blank">renaissancewarszawa.pl</a>
									</span>
								<?php
								}
								?>
							</div>
							<ul class="navbar-nav social-nav">
								<li class="nav-item facebook">
									<a class="nav-link" target="_blank" href="https://www.facebook.com/RenaissanceWarsawAirportHotel/">Facebook</a>
								</li>
								<li class="nav-item linkedin">
									<a class="nav-link" target="_blank" href="https://www.linkedin.com/company/18105600/">LinkedIn</a>
								</li>
							</ul>
							<ul class="lang d-none">
								<?php pll_the_languages();?>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</nav><!-- #site-navigation -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">
