var UTILITIES = {

	trimText: function(str, length, ending) {

	    if (length == null) {
	    	length = 100;
	    }

	    if (ending == null) {
	    	ending = '...';
	    }

	    if (str.length > length) {
			let trimmedString = str.substr(0, length);
				trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(' ')));
	    	return trimmedString + ending;
	    } else {
	    	return str;
	    }
	},    

	isMobile: {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },

	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },

	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },

	    Opera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },

	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i);
	    },

	    any: function() {
	        return (UTILITIES.isMobile.Android() || UTILITIES.isMobile.BlackBerry() || UTILITIES.isMobile.iOS() || UTILITIES.isMobile.Opera() || UTILITIES.isMobile.Windows());
	    }
	}	
};

module.exports = {
    trimText : UTILITIES.trimText,
    isMobile : UTILITIES.isMobile
};
