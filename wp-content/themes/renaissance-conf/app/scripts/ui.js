const header = document.querySelector('.site-header');
const headerHeight = header.offsetHeight;
const content = document.querySelector('.site-content');
	  content.style.paddingTop = headerHeight + 'px';
const primaryMenu = document.getElementById('primary-menu');
const menuItems = [...document.querySelectorAll('[class*="menu-item-"]')];
const menuItemsActive = [...document.querySelectorAll('.current-menu-item')];
const bookingItem = `
				<li class="nav-item">
					<a href="#contact" class="btn btn-ren">Rezerwuj</a>
				</li>
				`;

var UI = {

	stickyHeader: function() {
		let scrollpos = window.scrollY;
		if(scrollpos > headerHeight) {
			header.classList.add('active');
		} else {
		    header.classList.remove('active');
		}
	},

	menuItem: function(items) {
		items.forEach(function(item, index) {
			item.classList += ' nav-item';
			item.children[0].setAttribute('class', 'nav-link');
		});
	},

	menuItemActive: function(items) {
		items.forEach(function(item, index) {
			item.classList += ' active';
		});
	},

	menuItemBooking: function(el) {
		el.insertAdjacentHTML('beforeend', bookingItem);
	},

	events: function() {
		window.addEventListener('scroll', UI.stickyHeader, false);
	},

	init: function() {
		UI.events();
		(menuItems !== null) ? UI.menuItem(menuItems) : '';
		(menuItemsActive !== null) ? UI.menuItemActive(menuItemsActive) : '';
		// (primaryMenu !== null) ? UI.menuItemBooking(primaryMenu) : '';
	}
};

module.exports = {
    init : UI.init
};
