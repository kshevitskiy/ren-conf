//** Components

const swiper = require('./swiper');
const ui = require('./ui');

window.onload = function() {
	swiper.init();
	ui.init();	
};

