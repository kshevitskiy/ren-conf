var util = require('./utilities');

const tablet = 921;
const mobile = 767;

const carousel = [document.querySelector('.gallery-carousel')];
const slider = [document.querySelector('.rooms-slider')];
const gallery = [...document.querySelectorAll('.room-gallery')];
const mobileSliders = [...document.querySelectorAll('.mobile-slider')];
const tabletSliders = [...document.querySelectorAll('.tablet-slider')];

const SWIPER = {

	carousel: function(selector) {
		selector.forEach(function(el, index) {
			el.insertAdjacentHTML('beforeend', '<div class="swiper-pagination carousel-pagination"></div>');
	        let swiper = new Swiper(el, {
	        	loop: true,
	            grabCursor: true,
	            slidesPerView: 2,
				pagination: {
					el: '.swiper-pagination',
					clickable: true
				},
				speed: 1000,
				autoplay: {
					delay: 500,
				},
				spaceBetween: 50,
				breakpoints: {
					768: {
						slidesPerView: 2,
						spaceBetween: 30
					},

					480: {
						autoplay: false,
						slidesPerView: 1,
						spaceBetween: 30
					},
				}
	        });
		});
	},

	slider: function(selector) {
		selector.forEach(function(el, index) {
			el.insertAdjacentHTML('beforeend', '<div class="swiper-pagination carousel-pagination"></div>');
	        let swiper = new Swiper(el, {
	        	// touchRatio: 0,
	            grabCursor: false,
	            slidesPerView: 1,
	            centeredMode: true,
				pagination: {
					el: '.swiper-pagination',
					clickable: true
				},
				spaceBetween: 90,
				breakpoints: {
					768: {
						spaceBetween: 30
					},

					480: {
						spaceBetween: 30
					},
				}
	        });
		});
	},

	gallery: function(gallery) {
		let galleries;

		gallery.forEach(function(el, index) {
			el.insertAdjacentHTML('beforeend', '<div class="swiper-pagination thumbs-pagination"></div>');
	        galleries = new Swiper(el, {
	        	effect: 'fade',
	            grabCursor: true,
	            slidesPerView: 1,
	            direction: 'horizontal',
	            nested: true,
				pagination: {
				el: '.thumbs-pagination',
				clickable: true,
					renderBullet: function (index, className) {
						let thumb = this.slides[index].firstElementChild.outerHTML;
						return '<span class="gallery-thumb ' + className + '">' + thumb + '</span>';
					},
				},
	        });
	    });
	},

    mobile: function(selector) {
        selector.forEach(function(el, index) {
            console.log(el);
            let swiper = new Swiper(el, {
                grabCursor: true,
                slidesPerView: 'auto',
                spaceBetween: 30,
                centeredSlides: true,
            });
        });
    },

    mobileAutoPlay: function(selector) {
        selector.forEach(function(el, index) {
            console.log(el);
            let swiper = new Swiper(el, {
                grabCursor: true,
                slidesPerView: 'auto',
                spaceBetween: 30,
                centeredSlides: true,
                loop: true,
				speed: 1500,
				autoplay: {
					delay: 0,
				},
            });
        });
    },

    mobileSlidersBuild: function(sliders) {
        if (util.isMobile.any()) {
            let sliderElements = [];

            sliders.forEach(function(slider, index) {
                let sliderWrapper = document.createElement('div');
                    sliderWrapper.setAttribute('class', 'swiper-slider');
                let slides = slider.childNodes;

                slider.className += ' swiper-wrapper';
                slider.parentNode.insertBefore(sliderWrapper, slider);
                sliderWrapper.appendChild(slider);

                slides.forEach(function(slide, index) {
                    slide.className += ' swiper-slide';
                });

                sliderElements.push(sliderWrapper);
            });

            return sliderElements;
        }
    },

	init: function() {

		(carousel[0] !== null) ? SWIPER.carousel(carousel) : '';
		(slider[0] !== null) ? SWIPER.slider(slider) : '';
		(gallery[0] !== null) ? SWIPER.gallery(gallery) : '';

        if (util.isMobile.any() && window.innerWidth < tablet) {
            SWIPER.mobile(SWIPER.mobileSlidersBuild(tabletSliders));
        }

        if (util.isMobile.any() && window.innerWidth < mobile) {
            SWIPER.mobileAutoPlay(SWIPER.mobileSlidersBuild(mobileSliders));
        }
	}
};

module.exports = {
    init : SWIPER.init
};
