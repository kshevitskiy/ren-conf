<?php
/**
 * Template Name: Homepage
 *
 * The template for displaying homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
				get_template_part( 'pages/home/sections/intro' );
				get_template_part( 'pages/home/sections/about' );
				get_template_part( 'pages/home/sections/rooms' );
				get_template_part( 'pages/home/sections/ballroom' );
				get_template_part( 'pages/home/sections/meetings' );
				get_template_part( 'pages/home/sections/meeting-app' );
				get_template_part( 'pages/home/sections/equipment' );
				get_template_part( 'pages/home/sections/map' );
				get_template_part( 'pages/home/sections/contact' );
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();