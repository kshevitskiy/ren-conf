<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Renaissance_Conf
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<nav class="navbar navbar-expand-lg navbar-dark bg-faded main-navigation align-items-start">
			<div class="container">
				<div class="u-full-width d-flex flex-column flex-lg-row justify-content-between">
					<a href="#" class="site-branding">
						<span class="navbar-brand">Renaissance</span>
					</a><!-- .site-branding -->

					<div class="d-flex flex-column align-items-center align-items-lg-end ">
						<div class="site-footer-topbar-wrapper">
							<div class="d-flex flex-column align-items-center flex-lg-row align-items-lg-center">
								<div class="contact-info">
									<span>
										telefon: <a href="tel:+48221647000">+48 22 164 7000</a>
										e-mail: <a href="mailto:konferencje@renaissance.waw.pl">konferencje@renaissance.waw.pl</a>
										<a href="http://www.renaissancewarszawa.pl/" target="_blank">renaissancewarszawa.pl</a>
									</span>
								</div>
								<ul class="navbar-nav social-nav">
									<li class="nav-item facebook">
										<a class="nav-link" target="_blank" href="https://www.facebook.com/RenaissanceWarsawAirportHotel/">Facebook</a>
									</li>
									<li class="nav-item linkedin">
										<a class="nav-link" target="_blank" href="https://www.linkedin.com/company/18105600/">LinkedIn</a>
									</li>
								</ul>
								<ul class="lang d-none">
									<?php pll_the_languages();?>
								</ul>
							</div>
						</div>

						<div class="site-footer-navbar-wrapper">
							<?php
								wp_nav_menu( array(
									'theme_location'  => 'menu-1',
									'menu_id'         => 'primary-menu-footer',
									'menu_class'      => 'navbar-nav text-uppercase',
									'container'		  => 'ul'
								) );
							?>
						</div>
					</div>
				</div>
			</div>
		</nav><!-- #site-navigation -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
	var showMoreTrigger = document.querySelectorAll('.show-more-trigger');
	var showMoreText = document.querySelectorAll('.show-more-text');

	function showText() {
		this.style.display = 'none';
		this.nextElementSibling.style.display = 'inline';
	}
	for(var i = 0; i < showMoreTrigger.length; i++) {
		showMoreText[i].style.display = 'none';
		showMoreTrigger[i].style.cursor = 'pointer';
		showMoreTrigger[i].addEventListener('click', showText, false);
	}
</script>

</body>
</html>
