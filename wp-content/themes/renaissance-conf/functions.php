<?php
/**
 * Renaissance Conf functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Renaissance_Conf
 */

if ( ! function_exists( 'renaissance_conf_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function renaissance_conf_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Renaissance Conf, use a find and replace
		 * to change 'renaissance-conf' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'renaissance-conf', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'renaissance-conf' ),
			'menu-2' => esc_html__( 'Social', 'renaissance-conf' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'renaissance_conf_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'renaissance_conf_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function renaissance_conf_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'renaissance_conf_content_width', 640 );
}
add_action( 'after_setup_theme', 'renaissance_conf_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function renaissance_conf_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'renaissance-conf' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'renaissance-conf' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'renaissance_conf_widgets_init' );

/** 
 * jQuery init
 */
function jquery_init() {  
    if (!is_admin()) {  
        wp_deregister_script('jquery');  
        // wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', FALSE, '1.11.0', TRUE);  
        // wp_enqueue_script('jquery');
    }  
}
add_action('init', 'jquery_init');

/**
 * Enqueue scripts and styles.
 */
function renaissance_conf_scripts() {	
	wp_enqueue_style( 'renaissance-conf-style', get_stylesheet_uri() );
	wp_enqueue_style( 'renaissance-conf-swiper', '//cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.0/css/swiper.min.css' );
	wp_enqueue_style( 'renaissance-conf-bootstrap', '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css' );	
	wp_enqueue_style( 'renaissance-conf-main-style', get_template_directory_uri() . '/css/style.css' );		

	wp_enqueue_script( 'renaissance-conf-swiper', '//cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.0/js/swiper.min.js', array(), '4.1.0', true );
	wp_enqueue_script( 'renaissance-conf-bootstrap-native', '//cdnjs.cloudflare.com/ajax/libs/bootstrap.native/2.0.21/bootstrap-native-v4.min.js', array(), '4.0.0', true );	
	wp_enqueue_script( 'renaissance-conf-main-js', get_template_directory_uri() . '/js/main.js', array(), '20180208', true );

	// wp_enqueue_script( 'renaissance-conf-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'renaissance_conf_scripts' );

/**
 * Remove type attribute
 */
function remove_type_attr($tag, $handle) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}
// add_filter('style_loader_tag', 'remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'remove_type_attr', 10, 2);

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Add custom html to nav
 */
function custom_menu_item ( $items, $args ) {
	$booking_text = 'Rezerwuj';
	if (get_locale() == 'en_GB') {
		$booking_text = 'Book now';
	}

	if ($args->theme_location == 'menu-1') {
		$items .= '<li class="menu-item"><a href="#contact" class="btn btn-ren">'. $booking_text .'</a></li>';
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'custom_menu_item', 10, 2 );

/*
 * Hide ACF
 */
function remove_acf_menu() {
	remove_menu_page('edit.php?post_type=acf');
}
add_action( 'admin_menu', 'remove_acf_menu', 999);


/*
 * Disable wp-embed js
 */
function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );


/**
 * Clear script tag
 */
function clean_script_tag($input) {
	$input = str_replace("type='text/javascript' ", '', $input);
	return str_replace("'", '"', $input);
}
add_filter('script_loader_tag', 'clean_script_tag');


/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );	
		$urls = array_diff( $urls, array( $emoji_svg_url ) );
 	}
	return $urls;
}

/**
 * CF7 configuration
 */
add_filter('wpcf7_form_elements', function( $content ) {
	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
    return $content;
});

add_filter('wpcf7_form_checkboxes', function( $content ) {
	$checkbox = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-checkbox(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);	
    return $checkbox;
});

add_filter('wpcf7_form_list_items', function( $content ) {
	$listItem = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-list-item(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);	
    return $listItem;
});

/**
 *
 * Register custom post types
 *
 */

/**
 * Room
 */
function rooms() {

	$labels = array(
		'name'                  => _x( 'Rooms', 'Post Type General Name', 'renaissance-conf' ),
		'singular_name'         => _x( 'Room', 'Post Type Singular Name', 'renaissance-conf' ),
		'menu_name'             => __( 'Rooms', 'renaissance-conf' ),
		'name_admin_bar'        => __( 'Room', 'renaissance-conf' ),
		'archives'              => __( 'Room Archives', 'renaissance-conf' ),
		'attributes'            => __( 'Item Rooms', 'renaissance-conf' ),
		'parent_item_colon'     => __( 'Parent Room:', 'renaissance-conf' ),
		'all_items'             => __( 'All Rooms', 'renaissance-conf' ),
		'add_new_item'          => __( 'Add New Room', 'renaissance-conf' ),
		'add_new'               => __( 'Add New', 'renaissance-conf' ),
		'new_item'              => __( 'New Room', 'renaissance-conf' ),
		'edit_item'             => __( 'Edit Room', 'renaissance-conf' ),
		'update_item'           => __( 'Update Room', 'renaissance-conf' ),
		'view_item'             => __( 'View Room', 'renaissance-conf' ),
		'view_items'            => __( 'View Rooms', 'renaissance-conf' ),
		'search_items'          => __( 'Search Room', 'renaissance-conf' ),
		'not_found'             => __( 'Not found', 'renaissance-conf' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'renaissance-conf' ),
		'featured_image'        => __( 'Featured Image', 'renaissance-conf' ),
		'set_featured_image'    => __( 'Set featured image', 'renaissance-conf' ),
		'remove_featured_image' => __( 'Remove featured image', 'renaissance-conf' ),
		'use_featured_image'    => __( 'Use as featured image', 'renaissance-conf' ),
		'insert_into_item'      => __( 'Insert into room', 'renaissance-conf' ),
		'uploaded_to_this_item' => __( 'Uploaded to this room', 'renaissance-conf' ),
		'items_list'            => __( 'Rooms list', 'renaissance-conf' ),
		'items_list_navigation' => __( 'Rooms list navigation', 'renaissance-conf' ),
		'filter_items_list'     => __( 'Filter rooms list', 'renaissance-conf' ),
	);
	$args = array(
		'label'                 => __( 'Room', 'renaissance-conf' ),
		'description'           => __( 'Post Type Description', 'renaissance-conf' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-building',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'room', $args );

}
add_action( 'init', 'rooms', 0 );

/**
 * Meeting
 */
function meetings() {

	$labels = array(
		'name'                  => _x( 'Meetings', 'Post Type General Name', 'renaissance-conf' ),
		'singular_name'         => _x( 'Meeting', 'Post Type Singular Name', 'renaissance-conf' ),
		'menu_name'             => __( 'Meetings', 'renaissance-conf' ),
		'name_admin_bar'        => __( 'Meeting', 'renaissance-conf' ),
		'archives'              => __( 'Meeting Archives', 'renaissance-conf' ),
		'attributes'            => __( 'Item Meetings', 'renaissance-conf' ),
		'parent_item_colon'     => __( 'Parent Meeting:', 'renaissance-conf' ),
		'all_items'             => __( 'All Meetings', 'renaissance-conf' ),
		'add_new_item'          => __( 'Add New Meeting', 'renaissance-conf' ),
		'add_new'               => __( 'Add New', 'renaissance-conf' ),
		'new_item'              => __( 'New Meeting', 'renaissance-conf' ),
		'edit_item'             => __( 'Edit Meeting', 'renaissance-conf' ),
		'update_item'           => __( 'Update Meeting', 'renaissance-conf' ),
		'view_item'             => __( 'View Meeting', 'renaissance-conf' ),
		'view_items'            => __( 'View Meetings', 'renaissance-conf' ),
		'search_items'          => __( 'Search Meeting', 'renaissance-conf' ),
		'not_found'             => __( 'Not found', 'renaissance-conf' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'renaissance-conf' ),
		'featured_image'        => __( 'Featured Image', 'renaissance-conf' ),
		'set_featured_image'    => __( 'Set featured image', 'renaissance-conf' ),
		'remove_featured_image' => __( 'Remove featured image', 'renaissance-conf' ),
		'use_featured_image'    => __( 'Use as featured image', 'renaissance-conf' ),
		'insert_into_item'      => __( 'Insert into meeting', 'renaissance-conf' ),
		'uploaded_to_this_item' => __( 'Uploaded to this meeting', 'renaissance-conf' ),
		'items_list'            => __( 'Meetings list', 'renaissance-conf' ),
		'items_list_navigation' => __( 'Meetings list navigation', 'renaissance-conf' ),
		'filter_items_list'     => __( 'Filter meetings list', 'renaissance-conf' ),
	);
	$args = array(
		'label'                 => __( 'Meeting', 'renaissance-conf' ),
		'description'           => __( 'Post Type Description', 'renaissance-conf' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-flag',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'meeting', $args );

}
add_action( 'init', 'meetings', 0 );

/**
 * Minify html markup
 */
class WP_HTML_Compression
{
    // Settings
    protected $compress_css = true;
    protected $compress_js = true;
    protected $info_comment = true;
    protected $remove_comments = true;

    // Variables
    protected $html;
    public function __construct($html)
    {
   	 if (!empty($html))
   	 {
   		 $this->parseHTML($html);
   	 }
    }
    public function __toString()
    {
   	 return $this->html;
    }
    protected function bottomComment($raw, $compressed)
    {
   	 $raw = strlen($raw);
   	 $compressed = strlen($compressed);
   	 
   	 $savings = ($raw-$compressed) / $raw * 100;
   	 
   	 $savings = round($savings, 2);
   	 
   	 return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
    }
    protected function minifyHTML($html)
    {
   	 $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
   	 preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
   	 $overriding = false;
   	 $raw_tag = false;
   	 // Variable reused for output
   	 $html = '';
   	 foreach ($matches as $token)
   	 {
   		 $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
   		 
   		 $content = $token[0];
   		 
   		 if (is_null($tag))
   		 {
   			 if ( !empty($token['script']) )
   			 {
   				 $strip = $this->compress_js;
   			 }
   			 else if ( !empty($token['style']) )
   			 {
   				 $strip = $this->compress_css;
   			 }
   			 else if ($content == '<!--wp-html-compression no compression-->')
   			 {
   				 $overriding = !$overriding;
   				 
   				 // Don't print the comment
   				 continue;
   			 }
   			 else if ($this->remove_comments)
   			 {
   				 if (!$overriding && $raw_tag != 'textarea')
   				 {
   					 // Remove any HTML comments, except MSIE conditional comments
   					 $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
   				 }
   			 }
   		 }
   		 else
   		 {
   			 if ($tag == 'pre' || $tag == 'textarea')
   			 {
   				 $raw_tag = $tag;
   			 }
   			 else if ($tag == '/pre' || $tag == '/textarea')
   			 {
   				 $raw_tag = false;
   			 }
   			 else
   			 {
   				 if ($raw_tag || $overriding)
   				 {
   					 $strip = false;
   				 }
   				 else
   				 {
   					 $strip = true;
   					 
   					 // Remove any empty attributes, except:
   					 // action, alt, content, src
   					 $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content);
   					 
   					 // Remove any space before the end of self-closing XHTML tags
   					 // JavaScript excluded
   					 $content = str_replace(' />', '/>', $content);
   				 }
   			 }
   		 }
   		 
   		 if ($strip)
   		 {
   			 $content = $this->removeWhiteSpace($content);
   		 }
   		 
   		 $html .= $content;
   	 }
   	 
   	 return $html;
    }
   	 
    public function parseHTML($html)
    {
   	 $this->html = $this->minifyHTML($html);
   	 
   	 if ($this->info_comment)
   	 {
   		 $this->html .= "\n" . $this->bottomComment($html, $this->html);
   	 }
    }
    
    protected function removeWhiteSpace($str)
    {
   	 $str = str_replace("\t", ' ', $str);
   	 $str = str_replace("\n",  '', $str);
   	 $str = str_replace("\r",  '', $str);
   	 
   	 while (stristr($str, '  '))
   	 {
   		 $str = str_replace('  ', ' ', $str);
   	 }
   	 
   	 return $str;
    }
}

function wp_html_compression_finish($html)
{
    return new WP_HTML_Compression($html);
}

function wp_html_compression_start()
{
    ob_start('wp_html_compression_finish');
}
add_action('get_header', 'wp_html_compression_start');