<?php
/**
 * The template for displaying equipment section on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$args = array(
	'page_id'				=> '50',
	'posts_per_page'        => '1',
);

// The Query
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

	while ( $query->have_posts() ) :
			$query->the_post();
			$section_title = get_field('section-title');
	?>

	<section class="section equipment-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-12 col-lg-11">
					<div class="row justify-content-between align-items-center">
						<div class="col-sm-12 col-md-12 col-lg-5 u-m-top u-m-bottom">
							<header class="section-header u-no-p-top u-no-p-bottom">
								<h2><?php echo $section_title; ?></h2>
							</header>
							<br>
							<div class="copy u-text-light-gray">
								<?php the_content(); ?>
							</div>
							<br>
							<div class="text-center">
								<a href="#contact" class="btn btn-lg btn-ren">Rezerwuj</a>
							</div>
						</div>

						<div class="col-sm-12 col-md-12 col-lg-6 text-center">
							<div class="row justify-content-center">
								<div class="col-sm-12 col-md-12 col-lg-9">
									<div class="features">
										<ul>
											<li class="feature feature-row">
												<i class="icon icon-projector"></i>
												<div class="feature__header">20 projektorów<br>3300 lumenów</div>
											</li>

											<li class="feature feature-row">
												<i class="icon icon-server"></i>
												<div class="feature__header">Sieć WiFi<br>54 Mb/s częstotliwość 5 GHz</div>
											</li>

											<li class="feature feature-row">
												<i class="icon icon-karaoke"></i>
												<div class="feature__header">bezprzewodowy system<br>przekazywania obrazu i dźwięku</div>
											</li>

											<li class="feature feature-row">
												<i class="icon icon-translate"></i>
												<div class="feature__header">kabina do tłumaczeń<br>symultanicznych</div>
											</li>
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
	endwhile; // End of the loop.

endif;
?>




