<?php 
/**
 * The template for displaying meeting-app section on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$args = array(
	'page_id'				=> '48',
	'posts_per_page'        => '1',
);

// The Query
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

	while ( $query->have_posts() ) : 
			$query->the_post();			
			$section_title = get_field('section-title');
			$video_oembed  = get_field('video');
	?>

	<section class="section meeting-app-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-12 col-lg-11">
					<div class="row justify-content-between align-items-center">
						<div class="col-sm-12 col-md-12 col-lg-6 text-center u-m-top u-m-bottom">
							<figure class="video-wrapper">
								<?php

								// use preg_match to find iframe src
								preg_match('/src="(.+?)"/', $video_oembed, $matches);
								$src = $matches[1];

								// add extra params to iframe src
								$params = array(
								    'controls'    => 0,
								    'hd'        => 1,
								    'autohide'    => 1
								);

								$new_src = add_query_arg($params, $src);
								$video_oembed = '<iframe src="'. $new_src .'"></iframe>';

								// echo $video_oembed
								echo $video_oembed;
								?>
							</figure>
						</div>

						<div class="col-sm-12 col-md-12 col-lg-5">
							<header class="section-header u-no-p-top u-no-p-bottom text-center">
								<h2><?php echo $section_title; ?></h2>
							</header>
							<div class="divider divider-sm"></div>
							<div class="row justify-content-center">
								<div class="col-sm-12 col-md-12 col-lg-11">
									<div class="copy text-center u-text-light-gray">
										<?php the_content(); ?>
									</div>								
								</div>
							</div>
						</div>					
					</div>				
				</div>
			</div>
		</div>
	</section>	

	<?php
	endwhile; // End of the loop.	

endif; 
?>



