<?php 
/**
 * The template for displaying contact section on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$args = array(
	'page_id'				=> '12',
	'posts_per_page'        => '1',
);

// The Query
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

	while ( $query->have_posts() ) : 
			$query->the_post();			
			$section_title    = get_field('section-title');
			$section_subtitle = get_field('section-subtitle');
	?>

	<section class="section contact-section" id="contact">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-12 col-lg-11">
					<header class="section-header u-no-p-bottom text-center">
						<h2><?php echo $section_title; ?></h2>					
						<h4 class="section-header-subtitle u-text-light-gray"><?php echo $section_subtitle; ?></h4>
					</header>
					<div class="divider divider-sm divider--transparent"></div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-12 col-lg-11">
					<?php echo do_shortcode('[contact-form-7 id="27" title="Booking form advanced"]'); ?>
				</div>
			</div>
		</div>


		<?php
			get_template_part( 'pages/home/sections/contact-banner' );
			// get_template_part( 'pages/home/sections/contact-person' );
		?>		
	</section>	

	<?php
	endwhile; // End of the loop.	

endif; 
?>

