<?php 
/**
 * The template for displaying map section on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$args = array(
	'page_id'				=> '10',
	'posts_per_page'        => '1',
);

// The Query
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

	while ( $query->have_posts() ) : 
			$query->the_post();			
			$section_title    = get_field('section-title');
			$location_bus 	  = get_field('bus');
			$location_tram 	  = get_field('tram');
			$location_geo 	  = get_field('geo');
			$location_map 	  = get_field('map');
	?>

	<section class="section map-section" id="map">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-12 col-lg-11">
					<header class="section-header u-no-p-bottom text-center">
						<h2><?php echo $section_title; ?></h2>
						<div class="divider divider-sm"></div>
						<h4 class="section-header-subtitle u-text-light-gray">
							<span class="inline-data">							
								<i class="icon icon-bus icon-sm icon-inline"></i><?php echo $location_bus; ?>
							</span>
							<span class="inline-data">							
								<i class="icon icon-tram icon-sm icon-inline"></i><?php echo $location_tram; ?>
							</span>
							<br>
							<span class="inline-data">							
								<i class="icon icon-placeholder icon-sm icon-inline"></i><?php echo $location_geo; ?>
							</span>																		
						</h4>
					</header>				
				</div>
			</div>
		</div>

		<div class="map-wrapper">
			<?php echo $location_map; ?>				
		</div>
	</section>	

	<?php
	endwhile; // End of the loop.	

endif; 
?>