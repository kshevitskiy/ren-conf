<?php
/**
 * The template for displaying banner subsection on homepage in contact section
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$banner_image 	  = get_field('banner-image');
$banner_title 	  = get_field('banner-title');
$banner_text 	  = get_field('banner-text');
?>

<section class="section sub-section contact-section-banner">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xs-12 col-md-12 col-lg-8">
				<a href="#contact">
					<div class="banner">
						<figure class="banner-image-wrapper">
							<img src="<?php echo $banner_image['url']; ?>" alt="<?php echo $banner_image['alt']; ?>" class="banner__image" />
						</figure>
						<div class="banner-copy-wrapper">
							<h3 class="banner__title">
								<?php echo $banner_title; ?>
							</h3>
							<div class="copy u-text-light-gray">
								<p><?php echo $banner_text; ?></p>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>