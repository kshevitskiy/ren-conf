<?php 
/**
 * The template for displaying intro section on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */
?>

<section class="section intro-section">
	<div class="container">		
		<div class="row justify-content-center align-items-center">
			<div class="col-sm-12 col-md-12 col-lg-6">
				<header class="section-header">
					<h2 class="highlighted">
					<?php if (get_locale() == 'en_GB') { ?>
						<span class="font-weight-bold">R.E.N. Meetings</span><br>
						<span>The space for your</span><br>
						<span>event.</span>					
					<?php
					} else {
					?>
						<span class="font-weight-bold">R.E.N. Meetings</span><br>
						<span>Przestrzeń na Twoje</span><br>
						<span>wydarzenie.</span>
					<?php
					}
					?>
					</h2>
				</header>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-5 d-flex justify-content-center justify-content-lg-end">
				<?php echo do_shortcode('[contact-form-7 id="24" title="Booking form"]'); ?>
			</div>
		</div>
	</div>
</section>
