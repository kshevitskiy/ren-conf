<?php
/**
 * The template for displaying rooms section on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$args = array(
	'post_type'      => array( 'room' ),
	'post_status'    => array( 'publish' ),
    'posts_per_page' => -1,
    'order' 		 => 'ASC',
);

// The Query
$rooms = new WP_Query( $args );
?>

<section class="section rooms-section" id="rooms">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-12 col-md-12 col-lg-10">
				<header class="section-header u-no-p-bottom text-center">
					<h2><strong>Wyjątkowa przestrzeń</strong><br>blisko lotniska</h2>
				</header>
				<div class="divider"></div>
			</div>
		</div>
	</div>

	<?php
	if ( $rooms->have_posts() ) :
	?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-12 col-lg-11">
					<div class="rooms-slider">
						<ul class="swiper-wrapper">

							<?php
							while ( $rooms->have_posts() ) :
									$rooms->the_post();
									$images    = get_field('gallery-images', $post->ID);
									$size 	   = 'full';
									$area 	   = get_field('room-area');
									$equipment = get_field('room-equipment');
									$file      = get_field('download-file');
							?>
							<li class="room swiper-slide">
								<div class="row">
									<div class="col-sm-12 col-md-12 col-lg-6">
										<div class="room-gallery">
											<ul class="swiper-wrapper">
										        <?php
										        foreach( $images as $image ) :
										        ?>
										            <li class="swiper-slide gallery-slide">
										            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
										            </li>
										        <?php
										        endforeach;
										        ?>
											</ul>
										</div>
									</div>
									<div class="room-content col-sm-12 col-md-12 col-lg-6 d-flex flex-column justify-content-between">
										<h3 class="room-title u-text-light-gray text-uppercase">
											<?php the_title(); ?>
										</h3>
										<div class="room-meta">
											<div class="d-flex justify-content-between align-items-end flex-wrap flex-lg-nowrap">
												<ul class="features-row d-flex align-items-center justify-content-between text-center u-text-light-gray">
													<li class="feature">
														<div class="feature__header">powierzchnia <?php echo $area; ?></div>
														<i class="icon icon-plans"></i>
													</li>

													<li class="feature">
														<div class="feature__header">profesjonalne wyposażenie</div>
														<i class="icon <?php echo $equipment; ?>"></i>
													</li>
												</ul>
												<a href="<?php echo $file; ?>" class="btn btn-ren-gray btn-with-icon btn-lg" target="_blank">
													<span>Pobierz plany</span>
													<i class="icon icon-pdf icon-md"></i>
												</a>
											</div>
										</div>

										<?php
										if( have_rows('table') ) :
										?>
											<div class="room-details">
												<ul class="flex-table">

													<?php while( have_rows('table') ): the_row();
														// vars
														$title = get_sub_field('column-title');
														$value = get_sub_field('column-value');
													?>

													<li class="flex-table-col">
														<div class="flex-table-col-header"><?php echo $title; ?></div>
														<div class="flex-table-col-data"><?php echo $value; ?></div>
													</li>

													<?php endwhile; ?>

												</ul>
											</div>
										<?php
										endif;
										?>

									</div>
								</div>
							</li>
							<?php
							endwhile; // End of the loop.
							?>

						</ul>
					</div>
				</div>
			</div>
		</div>
	<?php
	endif;
	?>

</section>