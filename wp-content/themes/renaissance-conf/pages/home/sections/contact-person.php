<?php 
/**
 * The template for displaying contact person subsection on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$person_photo 	  = get_field('person-photo');
$person_name 	  = get_field('person-name');
$person__position = get_field('person-position');
$person_tel 	  = get_field('person-tel');
$person_email 	  = get_field('person-email');
$company_phone 	  = get_field('company-phone');
$company_email 	  = get_field('company-email');
?>

<section class="section sub-section contact-section-consultant">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-12 col-md-12 col-lg-11">
				<header class="section-header u-no-p-bottom text-center">
					<h2><strong>Skontaktuj</strong> się z nami</h2>					
				</header>
				<div class="divider divider-sm"></div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xs-12 col-md-12 col-lg-8">
				<div class="person d-flex">
					<figure class="person-image-wrapper">								
						<img src="<?php echo $person_photo['url']; ?>" alt="<?php echo $person_photo['alt']; ?>" class="person__image" />
					</figure>
					<div class="person-info-wrapper">							
						<div class="person-info-col">
							<div class="person-header-wrapper">								
								<h5 class="person__headline"><?php echo $person_name; ?></h5>
								<div class="person__position"><?php echo $person__position; ?></div>
							</div>
							<div class="person__tel">telefon: <?php echo $person_tel; ?></div>
							<div class="person__email">emial: <?php echo $person_email; ?></div>
						</div>

						<div class="person-info-col">
							<div class="person-header-wrapper">								
								<h5 class="person__headline">BIURO:</h5>
							</div>
							<div class="person__tel">telefon: <?php echo $company_phone; ?></div>
							<div class="person__email">emial: <?php echo $company_email; ?></div>
						</div>						
					</div>
				</div>
			</div>
		</div>			
	</div>
</section>	