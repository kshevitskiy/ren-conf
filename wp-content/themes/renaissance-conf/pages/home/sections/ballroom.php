<?php
/**
 * The template for displaying ballroom section on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$args = array(
	'page_id'				=> '46',
	'posts_per_page'        => '1',
);

// The Query
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

	while ( $query->have_posts() ) :
			$query->the_post();
			$section_title = get_field('section-title');
			$file = get_field('download-file');
	?>

	<section class="section ballroom-section" id="ballroom">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-12 col-lg-11">
					<div class="row justify-content-between">
						<div class="col-sm-12 col-md-12 col-lg-5 u-m-top">
							<header class="section-header u-no-p-bottom text-center">
								<h2><?php echo $section_title; ?></h2>
							</header>
							<div class="divider divider-sm"></div>
							<div class="row justify-content-center">
								<div class="col-sm-12 col-md-12 col-lg-11">
									<div class="copy text-center u-text-light-gray">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
							<div class="section-header text-center">
								<a href="<?php echo $file; ?>" class="btn btn-ren-gray btn-with-icon btn-lg" target="_blank">
									<span>Informator konferencyjny</span>
									<i class="icon icon-pdf icon-md"></i>
								</a>
							</div>
						</div>
						<div class="col-sm-12 col-md-12 col-lg-6 text-center u-m-top">
							<figure>
								<?php the_post_thumbnail(); ?>
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
	endwhile; // End of the loop.

endif;
?>