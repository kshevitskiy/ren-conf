<?php 
/**
 * The template for displaying meetings section on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$args = array(
	'page_id'				=> '8',
	'posts_per_page'        => '1',
);

// The Query
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

	while ( $query->have_posts() ) : 
			$query->the_post();			
			$section_title    = get_field('section-title');
			$section_subtitle    = get_field('section-subtitle');
	?>

	<section class="section meetings-section" id="meetings">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-12 col-lg-11">
					<header class="section-header u-no-p-bottom text-center">
						<h2><strong><?php echo $section_title; ?></strong></h2>
						<h4 class="section-header-subtitle u-text-light-gray"><?php echo $section_subtitle; ?></h4>
						<div class="section-header-copy">						
							<div class="copy u-text-light-gray">
								<?php the_content(); ?>
							</div>
						</div>
					</header>
					<div class="divider divider--transparent"></div>
				</div>

				<?php
				$args = array(
					'post_type'      => array( 'meeting' ),
					'post_status'    => array( 'publish' ),
				    'posts_per_page' => -1
				);

				// The Query
				$meetings = new WP_Query( $args );
				?>

				<?php
				if ( $meetings->have_posts() ) : 
				?>				

				<div class="col-sm-12 col-md-12 col-lg-11">					
					<ul class="meetings row tablet-slider">

						<?php
						while ( $meetings->have_posts() ) :
								$meetings->the_post();
						?>						
						<li class="col-sm-12 col-md-12 col-lg-4 meeting-slide">
							<div class="meeting">							
								<header class="meeting-header">
									<h3 class="meeting__title"><?php the_title(); ?></h3>
								</header>
								<div class="meeting-content">
									<figure>
										<?php the_post_thumbnail('full', array('class' => 'meeting__image')); ?>
									</figure>
									<div class="meeting-copy u-text-light-gray">
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						</li>
						<?php
						endwhile; // End of the loop.
						?>											

					</ul>
				</div>

				<?php
				endif; 
				?>				
			</div>
		</div>
	</section>

	<?php
	endwhile; // End of the loop.	

endif; 
?>