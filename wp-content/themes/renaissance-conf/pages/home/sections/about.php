<?php
/**
 * The template for displaying about section on homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Renaissance_Conf
 */

$args = array(
	'page_id'				=> '4',
	'posts_per_page'        => '1',
);

// The Query
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

	while ( $query->have_posts() ) :
			$query->the_post();
			$section_title    = get_field('section-title');
			$images			  = get_field('gallery-images', $post->ID);
			$size 			  = 'full';
	?>

		<section class="section about-section" id="about">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-sm-12 col-md-12 col-lg-10">
						<header class="section-header u-no-p-bottom text-center">
							<h2><?php echo $section_title; ?></h2>
						</header>
						<div class="divider"></div>
						<div class="copy text-center u-text-light-gray">
							<?php the_content(); ?>
						</div>
						<div class="divider"></div>
					</div>
				</div>
			</div>


			<div class="container">
				<?php
				if( $images ): ?>
					<div class="row justify-content-center">
						<div class="col-sm-12 col-md-12 col-lg-11">
							<div class="gallery-carousel">
								<ul class="swiper-wrapper">
						        <?php foreach( $images as $image ): ?>
						            <li class="swiper-slide">
						            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
						            </li>
						        <?php endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<div class="features">
					<ul class="d-flex justify-content-between text-center u-text-light-gray mobile-slider">
						<?php if (get_locale() == 'en_GB') { ?>
							<li class="feature">
								<div class="feature__header">great location</div>
								<i class="icon icon-placeholder"></i>
							</li>

							<li class="feature">
								<div class="feature__header">infrastructure</div>
								<i class="icon icon-presentation"></i>
							</li>

							<li class="feature">
								<div class="feature__header">consultant</div>
								<i class="icon icon-consultant"></i>
							</li>

							<li class="feature">
								<div class="feature__header">new technologies</div>
								<i class="icon icon-server"></i>
							</li>

							<li class="feature">
								<div class="feature__header">fast and free wi-fi</div>
								<i class="icon icon-wifi"></i>
							</li>

							<li class="feature">
								<div class="feature__header">amazing catering</div>
								<i class="icon icon-catering"></i>
							</li>

							<li class="feature">
								<div class="feature__header">parking for 102 cars</div>
								<i class="icon icon-car"></i>
							</li>
						<?php
						} else {
						?>
							<li class="feature">
								<div class="feature__header">dogodna lokalizacja</div>
								<i class="icon icon-placeholder"></i>
							</li>

							<li class="feature">
								<div class="feature__header">infrastruktura</div>
								<i class="icon icon-presentation"></i>
							</li>

							<li class="feature">
								<div class="feature__header">dedykowany konsultant</div>
								<i class="icon icon-consultant"></i>
							</li>

							<li class="feature">
								<div class="feature__header">nowoczesne technologie</div>
								<i class="icon icon-server"></i>
							</li>

							<li class="feature">
								<div class="feature__header">szybkie wi fi</div>
								<i class="icon icon-wifi"></i>
							</li>

							<li class="feature">
								<div class="feature__header">świetna kuchnia</div>
								<i class="icon icon-catering"></i>
							</li>

							<li class="feature">
								<div class="feature__header">102 miejsca parkingowe</div>
								<i class="icon icon-car"></i>
							</li>
						<?php
						}
						?>
					</ul>
				</div>
			</div>
		</section>

	<?php
	endwhile; // End of the loop.

endif;
?>
